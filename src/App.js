import "./App.css";
import logo from "./logo.svg";
import React, { Component } from "react";
// import NavbarComponent from "./component/NavbarComponent";
import { BrowserRouter, Route } from "react-router-dom";
import HomeContainer from "./container/HomeContainer";
import CreatePlayerContainer from "./container/CreatePlayerContainer";
import DetailPlayerContainer from "./container/DetailPlayerContainer";
import EditPlayerContainer from "./container/EditPlayerContainer";

export default class App extends Component {
  state = {
    players: false,
  };
  render() {
    return (
      <div>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>TEST | Gaea Crud USING ReactJS</p>
        </header>
        {/* <NavbarComponent /> */}
        <BrowserRouter>
          <Route path="/" exact>
            <HomeContainer players={this.state.players} />
          </Route>

          <Route path="/create" exact>
            <CreatePlayerContainer />
          </Route>

          <Route path="/detail/:username" exact>
            <DetailPlayerContainer />
          </Route>

          <Route path="/edit/:id" exact>
            <EditPlayerContainer />
          </Route>
        </BrowserRouter>
      </div>
    );
  }
}

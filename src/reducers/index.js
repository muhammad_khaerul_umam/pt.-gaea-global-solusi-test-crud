import { combineReducers } from "redux";
import players from "./players";
import { reducer as formReducer } from "redux-form";

export default combineReducers({
  players,
  form: formReducer,
});
